FROM php:7.4-fpm-alpine

WORKDIR /opt

COPY composer.json /opt/composer.json
COPY composer.lock /opt/composer.lock
COPY www /opt/www
COPY src /opt/src
RUN apk add composer --no-cache
RUN composer install --no-dev --no-cache