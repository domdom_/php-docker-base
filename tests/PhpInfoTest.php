<?php


use App\PhpInfo;
use PHPUnit\Framework\TestCase;

class PhpInfoTest extends TestCase
{
    private $sut;
    protected function setUp(): void
    {
        $this->sut = new PhpInfo();
    }

    public function testCanReturnPhpInfo()
    {
        $result = $this->sut->getInfo();

        self::assertEquals(phpinfo(),$result);
    }


}
