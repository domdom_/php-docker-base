<?php

namespace App;

class PhpInfo
{
    private $info;
    public function getInfo()
    {
        $this->info  = phpinfo();
        return $this->info;
    }
}