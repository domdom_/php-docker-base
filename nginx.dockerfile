FROM nginx:latest

COPY ./config/site.conf /etc/nginx/conf.d/default.conf
COPY www /opt/www